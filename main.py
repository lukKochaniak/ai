import argparse
import timeit
from algorhitms.AST import ast
from algorhitms.BFS import bfs
from algorhitms.DFS import dfs
from algorhitms.IDFS import idfs
from algorhitms.BestFirstStrategy import bestFirstStrategy
from algorhitms.SMA import sma
from common.resultUtil import export
from common.configurationUtil import readListConfiguration, readBoard, readOrder, readHeuristic
import CONSTANTS
from gui import PuzzleDemo


def main():
    parser = argparse.ArgumentParser()

    parser.add_argument('algorithm')
    parser.add_argument("-o", "--order", help="Searching order", required=False, default='DULR')
    parser.add_argument("-he", "--heuristic", help="Number of heuristic id, type 1 for manhattan, type 2 for displacedTiles", required=False, default=1)
    parser.add_argument("-r", "--rows", type=int, help="Number of rows", required=False, default=3)
    parser.add_argument("-c", "--columns", type=int, help="Number of columns", required=False, default=3)
    parser.add_argument("-b", "--board", help="Board", required=False, default='')
    parser.add_argument("-l", "--list", help="Int list (single) or double list (double)", required=False,
                        default='double')
    args = parser.parse_args()

    readListConfiguration(args.list)
    readBoard(args.board, args.rows, args.columns)
    readOrder(args.order)
    readHeuristic(int(args.heuristic))

    function = function_map[args.algorithm]

    start = timeit.default_timer()
    frontier = function(CONSTANTS.initial_state)
    stop = timeit.default_timer()

    CONSTANTS.final_time = stop - start

    export(frontier, stop - start)

    PuzzleDemo().mainloop()


function_map = {
    'bfs': bfs,
    'dfs': dfs,
    'ast': ast,
    'idfs': idfs,
    'bestFirstStrategy': bestFirstStrategy,
    'sma': sma
}

if __name__ == '__main__':
    main()
