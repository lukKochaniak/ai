from tkinter import *
from tkinter import ttk
import tkinter.font as font
import CONSTANTS


class PuzzleDemo(Frame):
    demoPanel = None
    current_index = 0
    max_size = 0

    def __init__(self, isapp=True, name='puzzles'):
        Frame.__init__(self, name=name)
        self.pack(expand=Y, fill=BOTH)
        self.master.title('puzzles')
        self.isapp = isapp
        self._create_demo_panel()

    def _create_demo_panel(self):
        bgColor = 'gray80'
        self.demoPanel = Frame(self, borderwidth=2, relief=SUNKEN, background=bgColor, width=640, height=480)
        self.demoPanel.pack(side=TOP, pady=1, padx=1)

        # set button background to demoPanel background
        helv24 = font.Font(family='Helvetica', size=24, weight='bold')
        ttk.Style().configure('Puzzle.TButton', background=bgColor, font=helv24)

        reversed(CONSTANTS.stepsInFinalState)
        self.max_size = CONSTANTS.stepsInFinalState.__len__() - 1
        self.current_index = 0

        self.draw_board()

        self._create_move_buttons()

        self.add_label()

    def draw_board(self):
        if CONSTANTS.listConfiguration == 'single':
            board = CONSTANTS.stepsInFinalState[self.current_index].state
        else:
            board = self.make_single_list_from_double_list(CONSTANTS.stepsInFinalState[self.current_index].state)

        # board = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]

        for i in range(CONSTANTS.board_width * CONSTANTS.board_height):
            num = board[i]
            self.xypos = {}
            if CONSTANTS.board_height % 2 != 0 or CONSTANTS.board_width % 2 != 0:
                self.xypos[num] = (i % CONSTANTS.board_width * 1 / CONSTANTS.board_width,
                                   i // CONSTANTS.board_width * 1 / CONSTANTS.board_height)
            else:
                self.xypos[num] = (i % CONSTANTS.board_width * 1 / CONSTANTS.board_width,
                                   i // CONSTANTS.board_height * 1 / CONSTANTS.board_height)
            b = ttk.Button(text=num, style='Puzzle.TButton')
            b.place(in_=self.demoPanel, relx=self.xypos[num][0], rely=self.xypos[num][1],
                    relwidth=1 / CONSTANTS.board_width, relheight=1 / CONSTANTS.board_height)

    def add_label(self):
        iterator = 0
        drawMoves = ''
        while iterator < str(CONSTANTS.moves).__len__():
            drawMoves += str(CONSTANTS.moves)[iterator:iterator + 150] + '\n'
            iterator += 150
        centralLabel = ttk.Label(self, text='nodes expanded: ' + str(CONSTANTS.nodes_expanded) + '\nmax_fringe_size: ' +
                                            str(CONSTANTS.max_frontier_size) + '\nmax_search_depth: ' + str(CONSTANTS.max_search_depth) +
                                            '\nrunning time: ' + str(CONSTANTS.final_time) + '\ninitial_state: ' + str(CONSTANTS.initial_state) + '\nlast_visited_state: '
                                            + str(CONSTANTS.last_visited_node.state) + '\nmoves: ' + drawMoves
                                 )
        centralLabel.pack(side=BOTTOM, padx=5, pady=5)

    def _create_move_buttons(self):
        leftButton = ttk.Button(self, text="Move back", command=self.move_back)
        leftButton.pack(side=LEFT, pady=5, padx=5)

        rightButton = ttk.Button(self, text="Move forward", command=self.move_forward)
        rightButton.pack(side=RIGHT, pady=5, padx=5)

    def move_forward(self):
        if self.current_index - 1 >= 0:
            self.current_index = self.current_index - 1
            self.draw_board()

    def move_back(self):
        if self.current_index + 1 <= self.max_size:
            self.current_index = self.current_index + 1
            self.draw_board()

    def make_single_list_from_double_list(self, step):
        board = list()
        for i in range(0, CONSTANTS.board_height):
            for j in range(0, CONSTANTS.board_width):
                board.append(step[i][j])

        return board


if __name__ == '__main__':
    PuzzleDemo().mainloop()
