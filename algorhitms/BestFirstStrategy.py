import CONSTANTS
from common.common import expand
from common.state import State
from heuristics.manhattanHeuristic import heuristicManhattan
from heuristics.displacedTiles import heuristicDisplaced


def bestFirstStrategy(startState):
    heuristic = function_map[CONSTANTS.heuristicId]
    key = heuristic(startState)
    current_state = State(startState, None, None, 0, 0, key)
    boardVisited = set()
    optionsList = list()
    optionsList.append(current_state)
    boardVisited.add(current_state.map)

    while optionsList:
        optionsList.sort(key=lambda o: o.key)
        node = optionsList.pop(0)
        CONSTANTS.last_visited_node = node

        if node.state == CONSTANTS.goal_state:
            CONSTANTS.goal_node = node
            return optionsList

        posiblePaths = expand(node)

        for path in posiblePaths:
            thisPath = path.map[:]
            if thisPath not in boardVisited:
                path.key = heuristic(path.state)
                optionsList.append(path)
                boardVisited.add(path.map[:])
                if path.depth > CONSTANTS.max_search_depth:
                    CONSTANTS.max_search_depth = 1 + CONSTANTS.max_search_depth


function_map = {
    'heuristicManhattan': heuristicManhattan,
    'heuristicDisplaced': heuristicDisplaced
}
