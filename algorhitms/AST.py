import CONSTANTS
from common.common import expand
from common.state import State
from heuristics.manhattanHeuristic import heuristicManhattan
from heuristics.displacedTiles import heuristicDisplaced


def ast(startState):
    heuristic = function_map[CONSTANTS.heuristicId]
    key = heuristic(startState)
    boardVisited = set()
    current_state = State(startState, None, None, 0, 0, key)
    queue = []
    queue.append(current_state)
    boardVisited.add(current_state.map)

    while queue:
        queue.sort(key=lambda o: o.key)
        node = queue.pop(0)
        CONSTANTS.last_visited_node = node

        if node.state == CONSTANTS.goal_state:
            CONSTANTS.goal_node = node
            return queue

        posiblePaths = expand(node)
        for path in posiblePaths:
            thisPath = path.map[:]
            if thisPath not in boardVisited:
                path.key = heuristicManhattan(path.state) + path.depth
                queue.append(path)
                boardVisited.add(path.map[:])
                if path.depth > CONSTANTS.max_search_depth:
                    CONSTANTS.max_search_depth = 1 + CONSTANTS.max_search_depth


function_map = {
    'heuristicManhattan': heuristicManhattan,
    'heuristicDisplaced': heuristicDisplaced
}
