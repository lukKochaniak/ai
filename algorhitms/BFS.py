from collections import deque
from common.common import expand
from common.state import State
import CONSTANTS


def bfs(startState):

    boardVisited = set()
    queue = deque([State(startState, None, None, 0, 0, 0)])

    while queue:
        node = queue.popleft()
        CONSTANTS.last_visited_node = node
        boardVisited.add(node.map)

        if node.state == CONSTANTS.goal_state:
            CONSTANTS.goal_node = node
            return queue

        posiblePaths = expand(node)

        for path in posiblePaths:
            if path.map not in boardVisited:
                queue.append(path)
                boardVisited.add(path.map)
                if path.depth > CONSTANTS.max_search_depth:
                    CONSTANTS.max_search_depth = CONSTANTS.max_search_depth + 1

        if len(queue) > CONSTANTS.max_frontier_size:
            queue_size = len(queue)
            CONSTANTS.max_frontier_size = queue_size
