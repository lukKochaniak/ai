from common.common import State
from common.common import expand
import CONSTANTS


def dfs(startState):
    boardVisited = set()
    stack = list([State(startState, None, None, 0, 0, 0)])

    while stack:
        node = stack.pop()
        CONSTANTS.last_visited_node = node
        boardVisited.add(node.map)

        if node.state == CONSTANTS.goal_state:
            CONSTANTS.goal_node = node
            return stack

        posiblePaths = reversed(expand(node))

        for path in posiblePaths:
            if path.map not in boardVisited:
                stack.append(path)
                boardVisited.add(path.map)
                if path.depth > CONSTANTS.max_search_depth:
                    CONSTANTS.max_search_depth = 1 + CONSTANTS.max_search_depth

        if len(stack) > CONSTANTS.max_frontier_size:
            CONSTANTS.max_frontier_size = len(stack)
