from common.state import State
from common.common import expand
import CONSTANTS


def idfs(startState, current_search_depth=0):
    boardVisited = set()
    stack = list([State(startState, None, None, 0, 0, 0)])

    while stack:
        node = stack.pop()
        CONSTANTS.last_visited_node = node
        boardVisited.add(node.map)

        if node.state == CONSTANTS.goal_state:
            CONSTANTS.goal_node = node
            return stack

        if node.depth == current_search_depth:
            if stack:
                continue
            else:
                return idfs(CONSTANTS.initial_state, current_search_depth + 1)

        posiblePaths = reversed(expand(node))

        for path in posiblePaths:
            if path.map not in boardVisited:
                stack.append(path)
                boardVisited.add(path.map)
                if path.depth > CONSTANTS.max_search_depth:
                    CONSTANTS.max_search_depth = 1 + CONSTANTS.max_search_depth

        if len(stack) > CONSTANTS.max_frontier_size:
            CONSTANTS.max_frontier_size = len(stack)
    else:
        if CONSTANTS.max_search_depth < 100:
            return idfs(CONSTANTS.initial_state, current_search_depth + 1)
