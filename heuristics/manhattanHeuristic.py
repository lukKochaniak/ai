import CONSTANTS


def heuristicManhattan(state):
    return sum(abs(b % CONSTANTS.board_width - g % CONSTANTS.board_width) + abs(b // CONSTANTS.board_width - g // CONSTANTS.board_width)
               for b, g in ((state.index(i), CONSTANTS.goal_state.index(i)) for i in range(1, CONSTANTS.board_height*CONSTANTS.board_width)))
