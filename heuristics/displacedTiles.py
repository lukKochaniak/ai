import CONSTANTS


def heuristicDisplaced(state):
    score = 0
    for i in range(0, CONSTANTS.board_height):
        for j in range(0, CONSTANTS.board_width):
            if state[j + i * CONSTANTS.board_width] != 0:
                if state[j + i * CONSTANTS.board_width] != CONSTANTS.goal_state[j + i * CONSTANTS.board_width]:
                    score += 1
    return score
