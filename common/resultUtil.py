import CONSTANTS


def backtrace(current_node):
    while CONSTANTS.initial_state != current_node.state:
        if current_node.move == 'U':
            movement = 'Up'
        elif current_node.move == 'D':
            movement = 'Down'
        elif current_node.move == 'L':
            movement = 'Left'
        else:
            movement = 'Right'

        CONSTANTS.moves.insert(0, movement)
        CONSTANTS.stepsInFinalState.append(current_node)
        current_node = current_node.parent

    CONSTANTS.stepsInFinalState.append(current_node)
    return CONSTANTS.moves


def export(frontier, time):
    if CONSTANTS.goal_node is not None:
        CONSTANTS.isSolved = True
        moves = backtrace(CONSTANTS.goal_node)
        writeToFile(True, frontier, time, CONSTANTS.goal_node, moves)
    else:
        moves = backtrace(CONSTANTS.last_visited_node)
        writeToFile(False, frontier, time, CONSTANTS.last_visited_node, moves)


def writeToFile(isSolved, frontier, time, node, moves):
    file = open('output.txt', 'w')
    file.write("nodes_expanded: " + str(CONSTANTS.nodes_expanded))
    file.write("\nmax_in_queue_elements: " + str(CONSTANTS.max_frontier_size))
    file.write("\nsearch_depth: " + str(node.depth))
    file.write("\nmax_search_depth: " + str(CONSTANTS.max_search_depth))
    file.write("\nrunning_time: " + format(CONSTANTS.final_time, '.8f'))
    file.write("\ninitial_state: " + str(CONSTANTS.initial_state))
    file.write("\nlast_visited_state: " + str(CONSTANTS.last_visited_node.state))
    file.write("\npath: " + str(moves))
    file.write("\nmoves: " + str(len(moves)))
    if isSolved:
        file.write("\nfringe_size: " + str(len(frontier)))
    else:
        file.write("\nUNFORTUNATELY THE SOLUTION COULDNT BE FOUND")

    file.close()
