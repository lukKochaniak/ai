import CONSTANTS
import random


def readBoard(configuration, rows, columns):
    if CONSTANTS.listConfiguration == 'single':
        readSingleBoard(configuration, rows, columns)
    elif CONSTANTS.listConfiguration == 'double':
        readDoubleListBoard(configuration, rows, columns)


def readDoubleListBoard(configuration, rows, columns):
    board = [[0 for x in range(columns)] for y in range(rows)]
    goal_board = [[0 for x in range(columns)] for y in range(rows)]
    listValues = list()

    if configuration == '':
        for x in range(0, rows * columns):
            listValues.append(x)

        random.shuffle(listValues)
    else:
        listValues = configuration.split(",")

        if rows * columns == len(listValues):
            listValues.reverse()
        else:
            raise Exception(
                "Specified rows and columns are not coherent with typed board - specify properly rows and columns number")

    for i in range(0, rows):
        for j in range(0, columns):
            board[i][j] = int(listValues.pop())
            goal_board[i][j] = j + i * columns + 1
    goal_board[rows - 1][columns - 1] = 0
    CONSTANTS.board_height = rows
    CONSTANTS.board_width = columns
    CONSTANTS.initial_state = board
    CONSTANTS.goal_state = goal_board
    return


def readSingleBoard(configuration, rows, columns):
    board = [0 for x in range(columns * rows)]
    goal_board = [0 for x in range(columns * rows)]
    listValues = list()

    if configuration == '':
        for x in range(0, rows * columns):
            listValues.append(x)

        random.shuffle(listValues)
    else:
        listValues = configuration.split(",")
        if rows * columns == len(listValues):
            listValues.reverse()
        else:
            raise Exception(
                "Specified rows and columns are not coherent with typed board - specify properly rows and columns number")

    for i in range(0, rows):
        for j in range(0, columns):
            board[j + i * columns] = int(listValues.pop())
            goal_board[j + i * columns] = j + i * columns + 1
    goal_board[columns - 1 + (rows - 1) * columns] = 0
    CONSTANTS.board_height = rows
    CONSTANTS.board_width = columns
    CONSTANTS.initial_state = board
    CONSTANTS.goal_state = goal_board


def readOrder(searchingOrder):
    CONSTANTS.firstSearchingDirection = searchingOrder[0]
    CONSTANTS.secondSearchingDirection = searchingOrder[1]
    CONSTANTS.thirdSearchingDirection = searchingOrder[2]
    CONSTANTS.fourthSearchingDirection = searchingOrder[3]
    return


def readHeuristic(heuristicId):
    if heuristicId == 1:
        CONSTANTS.heuristicId = 'heuristicManhattan'
    elif heuristicId == 2:
        CONSTANTS.heuristicId = 'heuristicDisplaced'
    else:
        raise Exception("Heuristic ID is wrong, type another one!")


def readListConfiguration(listConfiguration):
    if listConfiguration == 'double':
        CONSTANTS.listConfiguration = 'double'
    elif listConfiguration == 'single':
        CONSTANTS.listConfiguration = 'single'
    else:
        raise Exception("List configuration is wrong: write 'single' or 'double'")
