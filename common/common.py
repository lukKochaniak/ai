import CONSTANTS
import copy
from common.state import State


def expand(node):
    CONSTANTS.nodes_expanded += 1

    neighbors = list()

    neighbors.append(
        State(move(node.state, CONSTANTS.firstSearchingDirection), node, CONSTANTS.firstSearchingDirection,
              node.depth + 1, node.cost + 1, 0))
    neighbors.append(
        State(move(node.state, CONSTANTS.secondSearchingDirection), node, CONSTANTS.secondSearchingDirection,
              node.depth + 1, node.cost + 1, 0))
    neighbors.append(
        State(move(node.state, CONSTANTS.thirdSearchingDirection), node, CONSTANTS.thirdSearchingDirection,
              node.depth + 1, node.cost + 1, 0))
    neighbors.append(
        State(move(node.state, CONSTANTS.fourthSearchingDirection), node, CONSTANTS.fourthSearchingDirection,
              node.depth + 1, node.cost + 1, 0))

    nodes = [neighbor for neighbor in neighbors if neighbor.state]

    return nodes


def move(state, position):
    if CONSTANTS.listConfiguration == 'double':
        return moveDoubleList(state, position)
    elif CONSTANTS.listConfiguration == 'single':
        return moveSingleList(state, position)


def moveDoubleList(state, position):
    new_state = copy.deepcopy(state)

    xpos, ypos = None, None
    for i in range(0, CONSTANTS.board_height):
        for j in range(0, CONSTANTS.board_width):
            if state[i][j] == 0:
                xpos = j
                ypos = i
                break
        if xpos is not None:
            break

    if position == 'U':  # Up
        if 1 <= ypos <= CONSTANTS.board_height - 1:
            temp = new_state[ypos - 1][xpos]
            new_state[ypos - 1][xpos] = new_state[ypos][xpos]
            new_state[ypos][xpos] = temp
            return new_state
        else:
            return None

    if position == 'D':  # Down
        if 0 <= ypos <= CONSTANTS.board_height - 2:
            temp = new_state[ypos + 1][xpos]
            new_state[ypos + 1][xpos] = new_state[ypos][xpos]
            new_state[ypos][xpos] = temp
            return new_state
        else:
            return None

    if position == 'L':  # Left
        if 1 <= xpos <= CONSTANTS.board_width - 1:
            temp = new_state[ypos][xpos - 1]
            new_state[ypos][xpos - 1] = new_state[ypos][xpos]
            new_state[ypos][xpos] = temp
            return new_state
        else:
            return None

    if position == 'R':  # Right
        if 0 <= xpos <= CONSTANTS.board_width - 2:
            temp = new_state[ypos][xpos + 1]
            new_state[ypos][xpos + 1] = new_state[ypos][xpos]
            new_state[ypos][xpos] = temp
            return new_state
        else:
            return None


def moveSingleList(state, position):
    new_state = copy.copy(state)
    # new_state = state[:]

    xpos, ypos = None, None
    for i in range(0, CONSTANTS.board_height):
        for j in range(0, CONSTANTS.board_width):
            if state[j + i * CONSTANTS.board_width] == 0:
                xpos = j
                ypos = i
                break
        if xpos is not None:
            break

    if position == 'U':  # Up
        if 1 <= ypos <= CONSTANTS.board_height - 1:
            temp = new_state[xpos + (ypos - 1) * CONSTANTS.board_width]
            new_state[xpos + (ypos - 1) * CONSTANTS.board_width] = new_state[xpos + ypos * CONSTANTS.board_width]
            new_state[xpos + ypos * CONSTANTS.board_width] = temp
            return new_state
        else:
            return None

    if position == 'D':  # Down
        if 0 <= ypos <= CONSTANTS.board_height - 2:
            temp = new_state[xpos + (ypos + 1) * CONSTANTS.board_width]
            new_state[xpos + (ypos + 1) * CONSTANTS.board_width] = new_state[xpos + ypos * CONSTANTS.board_width]
            new_state[xpos + ypos * CONSTANTS.board_width] = temp
            return new_state
        else:
            return None

    if position == 'L':  # Left
        if 1 <= xpos <= CONSTANTS.board_width - 1:
            temp = new_state[xpos - 1 + ypos * CONSTANTS.board_width]
            new_state[xpos - 1 + ypos * CONSTANTS.board_width] = new_state[xpos + ypos * CONSTANTS.board_width]
            new_state[xpos + ypos * CONSTANTS.board_width] = temp
            return new_state
        else:
            return None

    if position == 'R':  # Right
        if 0 <= xpos <= CONSTANTS.board_width - 2:
            temp = new_state[xpos + 1 + ypos * CONSTANTS.board_width]
            new_state[xpos + 1 + ypos * CONSTANTS.board_width] = new_state[xpos + ypos * CONSTANTS.board_width]
            new_state[xpos + ypos * CONSTANTS.board_width] = temp
            return new_state
        else:
            return None
