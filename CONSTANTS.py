from common.state import State

goal_state = [0, 1, 2, 3, 4, 5, 6, 7, 8]
goal_node = None
initial_state = list()
last_visited_node = State
board_height = 0
board_width = 0
listConfiguration = 'double'
isSolved = False
final_time = None

firstSearchingDirection = ''
secondSearchingDirection = ''
thirdSearchingDirection = ''
fourthSearchingDirection = ''

heuristicId = ''

nodes_expanded = 0
max_search_depth = 0
max_frontier_size = 0

moves = list()
costs = set()
stepsInFinalState = list()
